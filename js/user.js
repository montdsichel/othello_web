$(document).ready(function (){
  // page scroll
  $('a[href="#top"]').click(function(){
    $('html, body').animate({scrollTop:0},'slow');
    return false;
  });
  
  // popup link
  $('#jsDropMenu li').hover(function(){
    var target = $(this).attr('class');
    if($(this).hasClass(target)){
      $('ul#' + target + ':not(:animated)').stop().show();
    }
  },function(){
    $(this).children('ul').stop().hide();
  });

  // map link
  $('#map').mapster({
    singleSelect : true,
    clickNavigate : true,
    mapKey: 'area',
    fill : true,
    altImage : '../img/map.png',
    fillOpacity : 1,
  });
  
});
